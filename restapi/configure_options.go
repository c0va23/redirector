package restapi

import (
	"net/http"

	"github.com/go-openapi/swag"
	"github.com/rs/cors"

	"gitlab.com/c0va23/redirector/log"
	"gitlab.com/c0va23/redirector/memstore"
	"gitlab.com/c0va23/redirector/redisstore"
	"gitlab.com/c0va23/redirector/restapi/operations"
	"gitlab.com/c0va23/redirector/store"
)

//nolint:lll
var appOptions struct {
	StoreType     string `short:"s" long:"store-type" description:"Type of store engine" choice:"memory" choice:"redis" default:"memory"`
	RedisURI      string `long:"redis-uri" description:"Connection URI for Redis. Required if store-type equal redis"`
	RedisPoolSize int    `long:"redis-pool-size" description:"Redis pool size" default:"10"`
	BasicUsername string `long:"basic-username" short:"u" env:"BASIC_USERNAME" description:"Username for Basic auth" required:"true"`
	BasicPassword string `long:"basic-password" short:"p" env:"BASIC_PASSWORD" description:"Password for Basic auth." required:"true"`

	CORSOrigins []string `long:"cors-origins" description:"List valid CORS origins" default:"*"`

	LogErrorKey string `long:"log-error-key" env:"LOG_ERROR_KEY" description:"Log error key" default:"error"`
}

var builderLogger = log.NewLeveledLogger("builder")

func configureFlags(api *operations.RedirectorAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
	api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{
		{
			ShortDescription: "store",
			Options:          &appOptions,
		},
	}
}

func buildCORSOptions() cors.Options {
	return cors.Options{
		AllowedOrigins:   appOptions.CORSOrigins,
		AllowCredentials: true,
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodDelete,
		},
		AllowedHeaders: []string{
			"Authorization",
			"Content-Type",
		},
	}
}

func buildStore() store.Store {
	switch appOptions.StoreType {
	case "memory":
		return memstore.NewMemStore()
	case "redis":
		client, err := redisstore.BuildRedisPool(appOptions.RedisURI, appOptions.RedisPoolSize)
		if nil != err {
			builderLogger.Fatalf("Redis error: %s", err)
		}

		return redisstore.NewRedisStore(client)
	default:
		builderLogger.Fatalf("Unknown store type: %s", appOptions.StoreType)
		return nil
	}
}
