package log

import (
	"os"

	logrus "github.com/sirupsen/logrus"
)

const loggerField = "logger"

const timeFormat = "2006-01-02T15:04:05.000Z"

// NewLogger create new configured logger
func NewLogger(
	logger string,
	level logrus.Level,
) logrus.FieldLogger {
	l := &logrus.Logger{
		Out: os.Stderr,
		Formatter: &logrus.TextFormatter{
			DisableColors:   true,
			TimestampFormat: timeFormat,
		},
		Level: level,
	}

	return l.WithField(loggerField, logger)
}

var logger = NewLogger("logger", logrus.InfoLevel)

const defaultLoggerLevel = logrus.InfoLevel
const levelEnvvar = "LOG_LEVEL"

func loggerLevel() logrus.Level {
	loggerLevel, configured := os.LookupEnv(levelEnvvar)
	if !configured {
		return defaultLoggerLevel
	}

	level, err := logrus.ParseLevel(loggerLevel)
	if nil != err {
		logger.WithError(err).Fatalf("Invalid level error: %s", err)
	}

	return level
}

// NewLeveledLogger build logger with level configured with ennvar LOG_LEVEL
func NewLeveledLogger(logger string) logrus.FieldLogger {
	return NewLogger(logger, loggerLevel())
}

// SetErrorKey overide logrus.ErrorKey
func SetErrorKey(errorkKey string) {
	logrus.ErrorKey = errorkKey
}
