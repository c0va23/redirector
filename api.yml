swagger: "2.0"
info:
  description: Redirector configure API
  version: "1.0.0"
  title: "Redirector API"
  contact:
    email: "c0va23@gmail.com"
  license:
    name: "MIT"

tags:
- name: config
  description: Configuration API
- name: redirect
  description: Redirection handler

paths:
  /host_rules:
    get:
      summary: "List host rules"
      operationId: listHostRules
      tags:
      - config
      consumes:
      - application/json
      produces:
      - application/json
      security:
      - apiSecurity: []
      responses:
        200:
          description: "List host rules"
          schema:
            type: array
            items:
              $ref: "#/definitions/HostRules"
        500:
          $ref: '#/responses/InternalServerError'

    post:
      summary: Create host rules
      description: Create host rule if host rule not exists
      operationId: createHostRules
      tags:
      - config
      consumes:
      - application/json
      produces:
      - application/json
      security:
      - apiSecurity: []
      parameters:
        - name: hostRules
          in: body
          schema:
            $ref: '#/definitions/HostRules'
          required: true
      responses:
        200:
          description: HostRules created
          schema:
            $ref: '#/definitions/HostRules'
        400: { $ref: '#/responses/InvalidJSON' }
        409:
          description: Host rules already exists
        422: { $ref: '#/responses/InvalidData' }
        500:
          $ref: '#/responses/InternalServerError'

  /host_rules/{host}:
    parameters:
    - in: path
      name: host
      type: string
      required: true
      description: Target Host

    get:
      operationId: getHostRule
      summary: Get HostRule
      tags:
      - config
      consumes:
      - application/json
      produces:
      - application/json
      security:
      - apiSecurity: []
      responses:
        404: { $ref: '#/responses/NotFound' }
        200:
          description: HostRules with host
          schema:
            $ref: "#/definitions/HostRules"
        500:
          $ref: '#/responses/InternalServerError'

    put:
      summary: Update host rules
      description: Update host rule if it exists
      operationId: updateHostRules
      tags:
      - config
      consumes:
      - application/json
      produces:
      - application/json
      security:
      - apiSecurity: []
      parameters:
        - name: hostRules
          in: body
          schema:
            $ref: '#/definitions/HostRules'
          required: true
      responses:
        200:
          description: HostRules updated
          schema:
            $ref: '#/definitions/HostRules'
        400: { $ref: '#/responses/InvalidJSON' }
        404: { $ref: '#/responses/NotFound' }
        422: { $ref: '#/responses/InvalidData' }
        500:
          $ref: '#/responses/InternalServerError'

    delete:
      summary: Delete host rules
      description: Delete host rule if it exists
      operationId: deleteHostRules
      tags:
      - config
      consumes:
      - application/json
      produces:
      - application/json
      security:
      - apiSecurity: []
      responses:
        204:
          description: HostRules deleted
        404: { $ref: '#/responses/NotFound' }
        500:
          $ref: '#/responses/InternalServerError'

  /_healthcheck:
    get:
      summary: Check service status
      operationId: healthcheck
      description: |
        If the service can handle requests, it responds with code 200.
        Otherwise, it will respond with a status of 500.
      tags:
      - redirect
      responses:
        200:
          description: Service can handle requests.
        500:
          description: Service cannot handle requests.

responses:
  InternalServerError:
    description: Internal server error
    schema:
      $ref: '#/definitions/ServerError'
  NotFound:
    description: Not found
  InvalidData:
    description: Invalid data
  InvalidJSON:
    description: Invalid JSON

securityDefinitions:
  apiSecurity:
    type: basic

definitions:
  Target:
    type: object
    x-nullable: false
    properties:
      path:
        type: string
        x-nullable: false
      httpCode:
        type: number
        format: int32
        minimum: 300
        maximum: 399
        x-nullable: false
    required:
    - path
    - httpCode
    example:
      {
        path: "/target",
        httpCode: 301
      }

  HostRules:
    type: object
    x-nullable: false
    properties:
      host:
        type: string
        format: idn-hostname
        x-nullable: false
      defaultTarget:
        $ref: '#/definitions/Target'
      rules:
        type: array
        items:
          $ref: "#/definitions/Rule"
    required:
    - host
    - defaultTarget
    - rules
    example:
      {
        host: "example.org",
        defaultTarget: {
          path: "//example.com/target",
          httpCode: 301
        },
        rules: [
          {
            sourcePath: 'source-path',
            target: {
              path: "//example.com/target",
              httpCode: 301
            },
            activeFrom: '2018-03-15T12:00:00Z'
          }
        ]
      }

  Rule:
    type: object
    x-nullable: false
    properties:
      sourcePath:
        type: string
        description: "Regex for match source path"
        x-nullable: false
      resolver:
        type: string
        description: "Rule resolver"
        x-nullable: false
        default: simple
        enum:
        - simple
        - pattern
      target:
        $ref: '#/definitions/Target'
      activeFrom:
        type: string
        format: date-time
        x-nullable: true
      activeTo:
        type: string
        format: date-time
        x-nullable: true
    required:
    - sourcePath
    - target
    - resolver
    example:
      {
        sourcePath: 'source-path',
        target: {
          path: '//example.com/target',
          httpCode: 301
        },
        activeFrom: '2018-03-16T12:00:00Z'
      }

  ServerError:
    type: object
    properties:
      message:
        type: string
