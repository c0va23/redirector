package mocks_test

import (
	"testing"

	"gitlab.com/c0va23/redirector/store"
	"gitlab.com/c0va23/redirector/testutils/mocks"

	"github.com/stretchr/testify/assert"
)

func TestStoreMock(t *testing.T) {
	assert.Implements(t, (*store.Store)(nil), &mocks.StoreMock{})
}
