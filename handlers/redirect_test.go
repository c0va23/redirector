package handlers_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/redirector/handlers"
	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/store"
	"gitlab.com/c0va23/redirector/testutils/factories"
	"gitlab.com/c0va23/redirector/testutils/mocks"
)

func TestNewRedirectHandler(t *testing.T) {
	s := new(mocks.StoreMock)
	r := new(mocks.ResolverMock)
	h := handlers.NewRedirectHandler(s, r)

	assert.Implements(t, (*http.Handler)(nil), h)
}

func TestRedirectHandler_Handle_ServerError(t *testing.T) {
	a := assert.New(t)

	s := new(mocks.StoreMock)
	r := new(mocks.ResolverMock)
	h := handlers.NewRedirectHandler(s, r)

	err := fmt.Errorf("GetHostRulesErr")
	host := fake.DomainName()
	s.On("GetHostRules", host).Return(nil, err)

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/test", nil)
	req.Host = host

	h.ServeHTTP(rw, req)

	res := rw.Result()
	defer res.Body.Close()

	a.Equal(http.StatusInternalServerError, res.StatusCode)

	s.AssertExpectations(t)
}

func TestRedirectHandler_Handle_NotFound(t *testing.T) {
	a := assert.New(t)

	s := new(mocks.StoreMock)
	r := new(mocks.ResolverMock)
	h := handlers.NewRedirectHandler(s, r)

	host := fake.DomainName()
	s.On("GetHostRules", host).Return(nil, store.ErrNotFound)

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "/test", nil)
	req.Host = host

	h.ServeHTTP(rw, req)

	res := rw.Result()
	defer res.Body.Close()

	a.Equal(http.StatusNotFound, res.StatusCode)

	s.AssertExpectations(t)
}

func TestRedirectHandler_Handle_Success(t *testing.T) {
	a := assert.New(t)

	s := new(mocks.StoreMock)
	r := new(mocks.ResolverMock)
	h := handlers.NewRedirectHandler(s, r)

	path := factories.GeneratePath()
	hostRules := factories.HostRulesFactory.MustCreate().(models.HostRules)
	s.On("GetHostRules", hostRules.Host).Return(&hostRules, nil)
	r.On("Resolve", hostRules, path).Return(hostRules.DefaultTarget)

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", path, nil)
	req.Host = hostRules.Host

	h.ServeHTTP(rw, req)

	res := rw.Result()
	defer res.Body.Close()

	a.Equal(int(hostRules.DefaultTarget.HTTPCode), res.StatusCode)
	a.Equal(hostRules.DefaultTarget.Path, res.Header.Get("Location"))

	s.AssertExpectations(t)
}

func TestRedirectHandler_Handle_IDNASuccess(t *testing.T) {
	a := assert.New(t)

	s := new(mocks.StoreMock)
	r := new(mocks.ResolverMock)

	defer s.AssertExpectations(t)
	defer r.AssertExpectations(t)

	encodedHost := "xn--e1afmkfd.xn--p1ai"
	decodedHost := "пример.рф"

	path := factories.GeneratePath()

	hostRules := factories.HostRulesFactory.MustCreateWithOption(map[string]interface{}{
		"Host": decodedHost,
	}).(models.HostRules)
	s.On("GetHostRules", decodedHost).Return(&hostRules, nil)
	r.On("Resolve", hostRules, path).Return(hostRules.DefaultTarget)

	h := handlers.NewRedirectHandler(s, r)

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", path, nil)
	req.Host = encodedHost

	h.ServeHTTP(rw, req)

	res := rw.Result()
	defer res.Body.Close()

	a.Equal(int(hostRules.DefaultTarget.HTTPCode), res.StatusCode)
	a.Equal(hostRules.DefaultTarget.Path, res.Header.Get("Location"))
}

func TestRedirectHandler_Handle_IDNAError(t *testing.T) {
	a := assert.New(t)

	s := new(mocks.StoreMock)
	r := new(mocks.ResolverMock)

	defer s.AssertExpectations(t)
	defer r.AssertExpectations(t)

	encodedHost := "xn--0"

	path := factories.GeneratePath()

	h := handlers.NewRedirectHandler(s, r)

	rw := httptest.NewRecorder()
	req := httptest.NewRequest("GET", path, nil)
	req.Host = encodedHost

	h.ServeHTTP(rw, req)

	res := rw.Result()
	defer res.Body.Close()

	a.Equal(http.StatusInternalServerError, res.StatusCode)
}
