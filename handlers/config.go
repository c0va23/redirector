package handlers

import (
	"sort"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/restapi/operations/config"
	"gitlab.com/c0va23/redirector/restapi/operations/redirect"
	"gitlab.com/c0va23/redirector/store"
	"gitlab.com/c0va23/redirector/validators"
)

// ConfigHandlers implement methods into restapi
type ConfigHandlers struct {
	store store.Store
}

// NewConfigHandlers initialize new controller
func NewConfigHandlers(store store.Store) ConfigHandlers {
	return ConfigHandlers{
		store: store,
	}
}

// ListHostRulesHandler is handler for ListHostRules
func (configHandler *ConfigHandlers) ListHostRulesHandler(
	params config.ListHostRulesParams, // nolint:unparam
	_principal interface{}, // nolint:unparam
) middleware.Responder {
	listHostRules, err := configHandler.store.ListHostRules()

	switch err {
	case nil:
		sort.Slice(listHostRules, func(i, j int) bool {
			return listHostRules[i].Host < listHostRules[j].Host
		})

		return config.NewListHostRulesOK().
			WithPayload(listHostRules)
	default:
		return config.
			NewListHostRulesInternalServerError().
			WithPayload(&models.ServerError{Message: err.Error()})
	}
}

// CreateHostRulesHandler is handler for CreateHostRules
func (configHandler *ConfigHandlers) CreateHostRulesHandler(
	params config.CreateHostRulesParams,
	_principal interface{}, // nolint:unparam
) middleware.Responder {
	if hostRulesError := validators.ValidateHostRules(params.HostRules); hostRulesError != nil {
		return config.NewCreateHostRulesUnprocessableEntity().WithPayload(hostRulesError)
	}

	err := configHandler.store.CreateHostRules(params.HostRules)

	switch err {
	case nil:
		return config.
			NewCreateHostRulesOK().
			WithPayload(&params.HostRules)
	case store.ErrExists:
		return config.NewCreateHostRulesConflict()
	default:
		return config.
			NewCreateHostRulesInternalServerError().
			WithPayload(&models.ServerError{
				Message: err.Error(),
			})
	}
}

// UpdateHostRulesHandler is handler for UpdateHostRules
func (configHandler *ConfigHandlers) UpdateHostRulesHandler(
	params config.UpdateHostRulesParams,
	_principal interface{}, // nolint:unparam
) middleware.Responder {
	if hostRulesError := validators.ValidateHostRules(params.HostRules); hostRulesError != nil {
		return config.NewUpdateHostRulesUnprocessableEntity().WithPayload(hostRulesError)
	}

	err := configHandler.store.UpdateHostRules(params.Host, params.HostRules)

	switch err {
	case nil:
		return config.
			NewUpdateHostRulesOK().
			WithPayload(&params.HostRules)
	case store.ErrNotFound:
		return config.NewUpdateHostRulesNotFound().
			WithPayload(&models.ServerError{
				Message: err.Error(),
			})
	default:
		return config.
			NewUpdateHostRulesInternalServerError().
			WithPayload(&models.ServerError{
				Message: err.Error(),
			})
	}
}

// GetHostRulesHandler is handler for GetHostRules
func (configHandler *ConfigHandlers) GetHostRulesHandler(
	params config.GetHostRuleParams,
	_principal interface{}, // nolint:unparam
) middleware.Responder {
	hostRules, err := configHandler.store.GetHostRules(params.Host)

	switch err {
	case nil:
		return config.NewGetHostRuleOK().
			WithPayload(hostRules)
	case store.ErrNotFound:
		return config.NewGetHostRuleNotFound().
			WithPayload(&models.ServerError{
				Message: err.Error(),
			})
	default:
		return config.NewGetHostRuleInternalServerError().
			WithPayload(&models.ServerError{Message: err.Error()})
	}
}

// DeleteHostRulesHandler is handler for DeleteHostRules
func (configHandler *ConfigHandlers) DeleteHostRulesHandler(
	params config.DeleteHostRulesParams,
	_principal interface{}, // nolint:unparam
) middleware.Responder {
	err := configHandler.store.DeleteHostRules(params.Host)
	switch err {
	case nil:
		return config.NewDeleteHostRulesNoContent()
	case store.ErrNotFound:
		return config.NewDeleteHostRulesNotFound().
			WithPayload(&models.ServerError{
				Message: err.Error(),
			})
	default:
		return config.
			NewDeleteHostRulesInternalServerError().
			WithPayload(&models.ServerError{
				Message: err.Error(),
			})
	}
}

// HealthCheckHandler is handler for HealthCheck
func (configHandler *ConfigHandlers) HealthCheckHandler(
	params redirect.HealthcheckParams, // nolint:unparam
) middleware.Responder {
	err := configHandler.store.CheckHealth()

	switch err {
	case nil:
		return redirect.NewHealthcheckOK()
	default:
		return redirect.NewHealthcheckInternalServerError()
	}
}
