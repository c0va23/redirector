package validators

import (
	"fmt"
	"sort"

	"gitlab.com/c0va23/redirector/locales"
	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/resolvers"
)

const minHTTPCode = 300
const maxHTTPCode = 399

// Target field names
const (
	FieldTargetHTTPCode = "httpCode"
	FieldTargetPath     = "path"
)

// ValidateTarget return validation error list and valid true if invalid.
// Otherwise return nil list and valid false.
func ValidateTarget(target models.Target) (
	modelError models.ModelValidationError,
) {
	if !(minHTTPCode <= target.HTTPCode && target.HTTPCode <= maxHTTPCode) {
		modelError = addFieldError(modelError, FieldTargetHTTPCode, locales.ErrorsTargetHTTPCodeOutOfRange)
	}

	if target.Path == "" {
		modelError = addFieldError(modelError, FieldTargetPath, locales.ErrorsRequired)
	}

	return
}

func validateTargetPlaceholders(
	target models.Target,
	expectedPlaceholderNum int,
) (modelError models.ModelValidationError) {
	placeholders := resolvers.PlaceholderRegexp.FindAllString(target.Path, -1)
	if len(placeholders) != expectedPlaceholderNum {
		return addFieldError(
			modelError,
			FieldTargetPath,
			locales.ErrorsTargetPathMissedPlaceholder,
		)
	}

	sort.StringSlice(placeholders).Sort()

	for index, placeholder := range placeholders {
		if placeholder != fmt.Sprintf("{%d}", index) {
			return addFieldError(
				modelError,
				FieldTargetPath,
				locales.ErrorsTargetPathInvalidPlaceholderIndex,
			)
		}
	}

	return
}
