package validators_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/icrowley/fake"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/redirector/locales"
	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/testutils/factories"
	"gitlab.com/c0va23/redirector/validators"
)

func TestValidateRule_EmptyRule(t *testing.T) {
	a := assert.New(t)

	ruleError := validators.ValidateRule(models.Rule{}, nil)
	a.Equal(
		models.ModelValidationError{
			{
				Name: validators.FieldRuleResolver,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsRuleResolverUnknown},
				},
			},
			{
				Name: validators.FieldRuleSourcePath,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsRequired},
				},
			},
			{
				Name: fmt.Sprintf("%s.%s", validators.FieldRuleTarget, validators.FieldTargetHTTPCode),
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsTargetHTTPCodeOutOfRange},
				},
			},
			{
				Name: fmt.Sprintf("%s.%s", validators.FieldRuleTarget, validators.FieldTargetPath),
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsRequired},
				},
			},
		},
		ruleError,
	)
}

func TestValidateRule_SimpleResolver(t *testing.T) {
	a := assert.New(t)

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"Resolver": models.RuleResolverSimple,
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError(nil),
		ruleError,
	)
}

func TestValidateRule_ValidPatternWithValidTarget(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"Path": "https://example.com/posts/{0}",
		}).(models.Target)

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"Resolver":   models.RuleResolverPattern,
		"SourcePath": "/r/(\\d+)",
		"Target":     target,
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError(nil),
		ruleError,
	)
}

func TestValidateRule_ValidPatternWithUnorderedPlaceholders(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"Path": "https://example.com/groups/{1}/posts/{0}",
		}).(models.Target)

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"Resolver":   models.RuleResolverPattern,
		"SourcePath": "/r/(\\d+)-(\\d+)",
		"Target":     target,
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError(nil),
		ruleError,
	)
}

func TestValidateRule_ValidPatternWithoutPlaceholder(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"Path": "https://example.com/posts/",
		}).(models.Target)

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"Resolver":   models.RuleResolverPattern,
		"SourcePath": "/r/(\\d+)",
		"Target":     target,
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError{
			{
				Name: fmt.Sprintf("%s.%s", validators.FieldRuleTarget, validators.FieldTargetPath),
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsTargetPathMissedPlaceholder},
				},
			},
		},
		ruleError,
	)
}

func TestValidateRule_ValidPatternWithInvalidPlaceholder(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"Path": "https://example.com/posts/{1}",
		}).(models.Target)

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"Resolver":   models.RuleResolverPattern,
		"SourcePath": "/r/(\\d+)",
		"Target":     target,
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError{
			{
				Name: fmt.Sprintf("%s.%s", validators.FieldRuleTarget, validators.FieldTargetPath),
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsTargetPathInvalidPlaceholderIndex},
				},
			},
		},
		ruleError,
	)
}

func TestValidateRule_InvalidPattern(t *testing.T) {
	a := assert.New(t)

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"Resolver":   models.RuleResolverPattern,
		"SourcePath": "/r/(\\d+",
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError{
			models.FieldValidationError{
				Name: validators.FieldRuleSourcePath,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsRuleSourcePathInvalidPattern},
				},
			},
		},
		ruleError,
	)
}

func TestValidateRule_InvalidActive(t *testing.T) {
	a := assert.New(t)

	activeFrom, _ := strfmt.ParseDateTime("2018-01-01T00:00:00.000Z")
	activeTo, _ := strfmt.ParseDateTime("2017-12-31T23:59:59.99Z")

	rule, _ := factories.RuleFactory.MustCreateWithOption(map[string]interface{}{
		"ActiveFrom": &activeFrom,
		"ActiveTo":   &activeTo,
	}).(models.Rule)

	ruleError := validators.ValidateRule(rule, nil)

	a.Equal(
		models.ModelValidationError{
			models.FieldValidationError{
				Name: validators.FieldRuleActiveTo,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsRuleActiveToTooLate},
				},
			},
		},
		ruleError,
	)
}

// nolint:funlen
func TestHostRulesValidate_Uniq(t *testing.T) {
	a := assert.New(t)

	sourcePath := strings.Replace(fake.WordsN(3), " ", "/", -1)

	toggleTime := strfmt.DateTime(time.Now())

	doubleRulesValidateErrors := models.ModelValidationError{
		models.FieldValidationError{
			Name: validators.FieldRuleSourcePath,
			Errors: []models.ValidationError{
				{TranslationKey: locales.ErrorsRuleSourcePathNotUniq},
			},
		},
	}

	testCases := []struct {
		activeFrom      *strfmt.DateTime
		activeTo        *strfmt.DateTime
		otherActiveFrom *strfmt.DateTime
		otherActiveTo   *strfmt.DateTime
		expectedErrors  models.ModelValidationError
	}{
		{
			expectedErrors: doubleRulesValidateErrors,
		},
		{
			activeFrom:    &toggleTime,
			otherActiveTo: &toggleTime,
		},
		{
			activeTo:        &toggleTime,
			otherActiveFrom: &toggleTime,
		},
		{
			activeFrom: &toggleTime,
		},
		{
			activeTo: &toggleTime,
		},
		{
			otherActiveTo: &toggleTime,
		},
		{
			activeTo: &toggleTime,
		},
		{
			activeTo:       &toggleTime,
			otherActiveTo:  &toggleTime,
			expectedErrors: doubleRulesValidateErrors,
		},
		{
			activeFrom:      &toggleTime,
			otherActiveFrom: &toggleTime,
			expectedErrors:  doubleRulesValidateErrors,
		},
		{
			activeTo:        &toggleTime,
			activeFrom:      &toggleTime,
			otherActiveTo:   &toggleTime,
			otherActiveFrom: &toggleTime,
			expectedErrors:  doubleRulesValidateErrors,
		},
	}

	for _, testCase := range testCases {
		rule := factories.
			RuleFactory.
			MustCreateWithOption(map[string]interface{}{
				"SourcePath": sourcePath,
				"ActiveFrom": testCase.activeFrom,
				"ActiveTo":   testCase.activeTo,
			}).(models.Rule)

		otherRule := factories.
			RuleFactory.
			MustCreateWithOption(map[string]interface{}{
				"SourcePath": sourcePath,
				"ActiveFrom": testCase.otherActiveFrom,
				"ActiveTo":   testCase.otherActiveTo,
			}).(models.Rule)

		hostRulesErrors := validators.ValidateRule(
			rule,
			[]models.Rule{otherRule},
		)

		a.Equal(testCase.expectedErrors, hostRulesErrors)
	}
}
