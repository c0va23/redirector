package validators

import (
	"regexp"
	"time"

	"github.com/go-openapi/strfmt"
	"gitlab.com/c0va23/redirector/locales"
	"gitlab.com/c0va23/redirector/models"
)

// Rule field names
const (
	FieldRuleResolver   = "resolver"
	FieldRuleSourcePath = "sourcePath"
	FieldRuleTarget     = "target"
	FieldRuleActiveTo   = "activeTo"
)

// ValidateRule return nil if rule is valid or models.ModelValidationError if
// is invalid
func ValidateRule(rule models.Rule, otherRules []models.Rule) (
	modelError models.ModelValidationError,
) {
	if models.RuleResolverSimple != rule.Resolver &&
		models.RuleResolverPattern != rule.Resolver {
		modelError = addFieldError(modelError, FieldRuleResolver, locales.ErrorsRuleResolverUnknown)
	}

	if rule.SourcePath == "" {
		modelError = addFieldError(modelError, FieldRuleSourcePath, locales.ErrorsRequired)
	}

	if models.RuleResolverPattern == rule.Resolver {
		patternError, patternNum := validatePattern(rule)
		if patternError != nil {
			modelError = append(modelError, patternError...)
		} else {
			targetPlaceholderError := validateTargetPlaceholders(rule.Target, patternNum)
			modelError = addEmbedError(modelError, FieldRuleTarget, targetPlaceholderError)
		}
	}

	if targetError := ValidateTarget(rule.Target); targetError != nil {
		modelError = addEmbedError(modelError, FieldRuleTarget, targetError)
	}

	modelError = validateActive(modelError, (*time.Time)(rule.ActiveFrom), (*time.Time)(rule.ActiveTo))

	if uniqError := validateRulesUniq(rule, otherRules); uniqError != nil {
		modelError = append(modelError, uniqError...)
	}

	return modelError
}

func validatePattern(
	rule models.Rule,
) (
	modelError models.ModelValidationError,
	placeholderNum int,
) {
	pattern, errPattern := regexp.Compile(rule.SourcePath)
	if nil != errPattern {
		return addFieldError(
			modelError,
			FieldRuleSourcePath,
			locales.ErrorsRuleSourcePathInvalidPattern,
		), 0
	}

	return modelError, pattern.NumSubexp()
}

func validateActive(
	modelError models.ModelValidationError,
	activeFrom *time.Time,
	activeTo *time.Time,
) models.ModelValidationError {
	if nil != activeFrom && nil != activeTo && activeTo.Before(*activeFrom) {
		return addFieldError(modelError, FieldRuleActiveTo, locales.ErrorsRuleActiveToTooLate)
	}

	return modelError
}

func validateRulesUniq(
	rule models.Rule,
	previusRules []models.Rule,
) models.ModelValidationError {
	for _, otherRule := range previusRules {
		if rule.SourcePath != otherRule.SourcePath {
			continue
		}

		if !equalDateTimeRef(rule.ActiveFrom, otherRule.ActiveFrom) {
			continue
		}

		if !equalDateTimeRef(rule.ActiveTo, otherRule.ActiveTo) {
			continue
		}

		return models.ModelValidationError{
			models.FieldValidationError{
				Name: FieldRuleSourcePath,
				Errors: []models.ValidationError{
					{
						TranslationKey: locales.ErrorsRuleSourcePathNotUniq,
					},
				},
			},
		}
	}

	return nil
}

func equalDateTimeRef(dateA, dateB *strfmt.DateTime) bool {
	if nil == dateA && nil == dateB {
		return true
	}

	if nil != dateA && nil != dateB && *dateA == *dateB {
		return true
	}

	return false
}
