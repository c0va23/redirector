package validators_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/c0va23/redirector/locales"
	"gitlab.com/c0va23/redirector/models"
	"gitlab.com/c0va23/redirector/testutils/factories"
	"gitlab.com/c0va23/redirector/validators"
)

func TestValidateTarget_EmptyPath(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"Path": "",
		}).(models.Target)

	targetError := validators.ValidateTarget(target)

	a.Equal(
		models.ModelValidationError{
			{
				Name: validators.FieldTargetPath,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsRequired},
				},
			},
		},
		targetError,
	)
}

func TestValidateTarget_HTTCodeTooLow(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"HTTPCode": int32(299),
		}).(models.Target)

	targetError := validators.ValidateTarget(target)

	a.Equal(
		models.ModelValidationError{
			{
				Name: validators.FieldTargetHTTPCode,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsTargetHTTPCodeOutOfRange},
				},
			},
		},
		targetError,
	)
}

func TestValidateTarget_HTTCodeTooMuch(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"HTTPCode": int32(400),
		}).(models.Target)

	targetError := validators.ValidateTarget(target)

	a.Equal(
		models.ModelValidationError{
			{
				Name: validators.FieldTargetHTTPCode,
				Errors: []models.ValidationError{
					{TranslationKey: locales.ErrorsTargetHTTPCodeOutOfRange},
				},
			},
		},
		targetError,
	)
}

func TestValidateTarget_HTTCodeMinValue(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"HTTPCode": int32(300),
		}).(models.Target)

	targetError := validators.ValidateTarget(target)

	a.Equal(
		models.ModelValidationError(nil),
		targetError,
	)
}

func TestValidateTarget_HTTCodeMaxValue(t *testing.T) {
	a := assert.New(t)

	target := factories.
		TargetFactory.
		MustCreateWithOption(map[string]interface{}{
			"HTTPCode": int32(399),
		}).(models.Target)

	targetError := validators.ValidateTarget(target)

	a.Equal(
		models.ModelValidationError(nil),
		targetError,
	)
}
