# Redirector

[![pipeline status](https://gitlab.com/c0va23/redirector/badges/master/pipeline.svg)](https://gitlab.com/c0va23/redirector/commits/master)
[![coverage report](https://gitlab.com/c0va23/redirector/badges/master/coverage.svg)](https://gitlab.com/c0va23/redirector/commits/master)
[![Docker Repository on Quay](https://quay.io/repository/c0va23/redirector/status "Docker Repository on Quay")](https://quay.io/repository/c0va23/redirector)

## About `redirector`

`redirector` is dynamic configurable HTTP-redirection service.

Supported store engine:
- `memory` (not store configuration between restarts)
- `redis` (now support only single-server configuration)

Supported match / resolve algorithm:
- `simple` (full match of host and path, not change target)
- `pattern` (full match host, match path by regexp, replace target placeholders by pattern groups)


### Use cases

- Short links with full path name control
- Short links with activation codes (promo codes, confirmation of email addresses, password recovery, etc.)


### How it work?

1. Run service on your server(s) ([See usage](#Usage))
2. Configure service via [API](/api.yml) (Web UI expected in the future)
3. Direct the DNS records of the domains used by the redirector to the server where the service is started
4. Test by open any configured link in browser or with `curl`


### Algorithm matching rules and resolving targets

1. Find host by full match.
2. If host not found, then return empty response with HTTP code 404.
3. Find rule with selected resolve algorithm.
4. If rule not found, then redirect to default target url and HTTP code.
5. If rule found, then redirect to rule URL and HTTP code.


### Pattern resolver example

If we have: source `^/(.)$` and target path `https://example.org/promocode/{0}`.

Then request to path `/PROMOCODE` resolved to `https://example.org/promocode/PROMOCODE`.


### Ready for production use?

Already used in production since June 2018.


### Features

- Redirect on full host and path match
- Redirect on full match host and match the path pattern
- In-memory storage (single-instance mode only; temporary but fast storage)
- Redis storage (Multiple instance support)
- The possibility of implementing third-party repositories (via fork)
- Support IDNA host names
- Configuration via web UI (https://gitlab.com/c0va23/redirector-ui)


## Test and build

### Requirements

- `go` (tested on version 1.12)
- `make` (GNU Make 4.2.1)

`make` installs dependencies and utilities when needed. And it generates code when you need it.


### Run linters and tests

```bash
make lint
make test
```


### Build server binary

```bash
make bin/redirector-server-linux-amd64
make GOARCH=arm bin/redirector-server-linux-arm
make GOARCH=arm64 bin/redirector-server-linux-arm64
```


## Build docker image and run container

```bash
# Build image
docker build -t redirector .

# Run container
export USERNAME=user
export PASSWORD=pass
export PORT=8080

docker run -p $PORT:8080 -e USERNAME=$USERNAME -e PASSWORD=$PASSWORD --name redirector --rm -d redirector

curl "http://localhost:$PORT/host_rules" --user $USERNAME:$PASSWORD --basic -H 'content-type: application/json' --data '{"host":"example.org","defaultTarget":{"path":"https://example.org/","httpCode":301},"rules":[]}'
# {"defaultTarget":{"httpCode":301,"path":"https://example.org/"},"host":"example.org","rules":[]}

curl -v -H 'Host: example.org' "http://localhost:$PORT/"
# *   Trying ::1...
# * TCP_NODELAY set
# * Connected to localhost (::1) port 3030 (#0)
# > GET / HTTP/1.1
# > Host: example.org
# > User-Agent: curl/7.64.0
# > Accept: */*
# >
# < HTTP/1.1 301 Moved Permanently
# < Content-Type: application/json
# < Location: https://example.org/
# < Vary: Origin
# < Date: Thu, 21 Mar 2019 19:06:33 GMT
# < Content-Length: 0
# <
# * Connection #0 to host localhost left intact

docker kill redirector
```


## Usage

Show usage with command:
```bash
./bin/redirector-server-linux-amd64 --help
```

```
Usage:
  redirector-server-linux-amd64 [OPTIONS]

Redirector configure API

Application Options:
      --scheme=                   the listeners to enable, this can be repeated and defaults to the schemes in the swagger spec
      --cleanup-timeout=          grace period for which to wait before killing idle connections (default: 10s)
      --graceful-timeout=         grace period for which to wait before shutting down the server (default: 15s)
      --max-header-size=          controls the maximum number of bytes the server will read parsing the request header's keys and values, including the request line. It does not limit the
                                  size of the request body. (default: 1MiB)
      --socket-path=              the unix socket to listen on (default: /var/run/redirector.sock)
      --host=                     the IP to listen on (default: localhost) [$HOST]
      --port=                     the port to listen on for insecure connections, defaults to a random value [$PORT]
      --listen-limit=             limit the number of outstanding requests
      --keep-alive=               sets the TCP keep-alive timeouts on accepted connections. It prunes dead TCP connections ( e.g. closing laptop mid-download) (default: 3m)
      --read-timeout=             maximum duration before timing out read of the request (default: 30s)
      --write-timeout=            maximum duration before timing out write of the response (default: 60s)
      --tls-host=                 the IP to listen on for tls, when not specified it's the same as --host [$TLS_HOST]
      --tls-port=                 the port to listen on for secure connections, defaults to a random value [$TLS_PORT]
      --tls-certificate=          the certificate to use for secure connections [$TLS_CERTIFICATE]
      --tls-key=                  the private key to use for secure conections [$TLS_PRIVATE_KEY]
      --tls-ca=                   the certificate authority file to be used with mutual tls auth [$TLS_CA_CERTIFICATE]
      --tls-listen-limit=         limit the number of outstanding requests
      --tls-keep-alive=           sets the TCP keep-alive timeouts on accepted connections. It prunes dead TCP connections ( e.g. closing laptop mid-download)
      --tls-read-timeout=         maximum duration before timing out read of the request
      --tls-write-timeout=        maximum duration before timing out write of the response

store:
  -s, --store-type=[memory|redis] Type of store engine (default: memory)
      --redis-uri=                Connection URI for Redis. Required if store-type equal redis
      --redis-pool-size=          Redis pool size (default: 10)
  -u, --basic-username=           Username for Basic auth [$BASIC_USERNAME]
  -p, --basic-password=           Password for Basic auth. [$BASIC_PASSWORD]
      --cors-origins=             List valid CORS origins (default: *)

Help Options:
  -h, --help                      Show this help message
```

### Logger level

Logger level can be configured with ennvar LOG_LEVEL.

Allowed values:
- debug
- info
- warn
- error
- fatal
- panic
