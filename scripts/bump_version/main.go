///usr/bin/env go run "$0" "$@"; exit $?
package main

import (
	"fmt"
	"os"
)

const versionFormat = "v%d.%d.%d\n"

func main() {
	scriptPath := os.Args[0]

	if len(os.Args) < 3 {
		panic(fmt.Sprintf("Run as:\n%s VERSION <level>", scriptPath))
	}

	versionFilePath := os.Args[1]
	level := os.Args[2]

	versionFile, err := os.OpenFile(versionFilePath, os.O_RDWR, os.ModePerm)
	if nil != err {
		panic(err)
	}

	var (
		major uint
		minor uint
		patch uint
	)

	_, err = fmt.Fscanf(versionFile, versionFormat, &major, &minor, &patch)
	if nil != err {
		panic(err)
	}

	switch level {
	case "major":
		major++

		minor = 0
		patch = 0
	case "minor":
		minor++

		patch = 0
	case "patch":
		patch++
	default:
		panic(fmt.Sprintf("Unknown level: %s", level))
	}

	newVersion := fmt.Sprintf(versionFormat, major, minor, patch)
	fmt.Printf("New version: %s\n", newVersion)

	if _, err := versionFile.Seek(0, 0); nil != err {
		panic(err)
	}

	if _, err := fmt.Fprint(versionFile, newVersion); nil != err {
		panic(err)
	}
}
