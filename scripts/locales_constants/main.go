//usr/bin/env go run "$0" "$@"; exit $?

package main

import (
	"flag"
	"os"
	"regexp"
	"strings"
	"text/template"

	"github.com/BurntSushi/toml"
)

type traslationsType map[string]map[string]string

type templateDateType struct {
	Translations traslationsType
	PackageName  string
}

var config struct {
	localesFilePath string
	targetFilePath  string
	pacakgeName     string
}

func parseConfig() {
	flag.StringVar(&config.localesFilePath, "locales", "", "Locales file path")
	flag.StringVar(&config.targetFilePath, "target", "", "Target file path")
	flag.StringVar(&config.pacakgeName, "package", "locales", "Target package name")
	flag.Parse()
}

var partSeparator = regexp.MustCompile(`\.|_`)
var formatReplaces = map[string]string{
	"Http": "HTTP",
}

func formatKey(key string) string {
	parts := partSeparator.Split(key, -1)
	formatedParts := make([]string, 0, len(parts))

	for _, part := range parts {
		formatedPart := strings.ToUpper(part[0:1]) + part[1:]
		for source, target := range formatReplaces {
			formatedPart = strings.Replace(formatedPart, source, target, 1)
		}

		formatedParts = append(formatedParts, formatedPart)
	}

	return strings.Join(formatedParts, "")
}

var targetTemplate = template.Must(template.New("").
	Funcs(map[string]interface{}{
		"formatKey": formatKey,
	}).
	Parse(`// Code generated. DO NOT EDIT

package {{ .PackageName }}

{{ range $group, $translations := .Translations -}}
{{- range $key, $translation := $translations -}}
// {{ formatKey $group }}{{ formatKey $key }} alias for {{ $group }}.{{ $key }}
const {{ formatKey $group }}{{ formatKey $key }} = "{{ $group }}.{{ $key }}"
{{ end -}}
{{- end -}}`))

func close(fn func() error) {
	if err := fn(); nil != err {
		panic(err)
	}
}

func main() {
	parseConfig()

	localesFile, err := os.Open(config.localesFilePath)
	if nil != err {
		panic(err)
	}

	defer close(localesFile.Close)

	targetFile, err := os.OpenFile(config.targetFilePath, os.O_CREATE|os.O_WRONLY, 0644)
	if nil != err {
		panic(err)
	}

	defer close(targetFile.Close)

	if err = targetFile.Truncate(0); nil != err {
		panic(err)
	}

	templateDate := templateDateType{
		Translations: make(traslationsType),
		PackageName:  config.pacakgeName,
	}

	_, err = toml.DecodeReader(localesFile, &templateDate.Translations)
	if nil != err {
		panic(err)
	}

	if err = targetTemplate.Execute(targetFile, templateDate); nil != err {
		panic(err)
	}
}
