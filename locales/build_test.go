package locales_test

import (
	"testing"

	"gitlab.com/c0va23/redirector/locales"
	"gitlab.com/c0va23/redirector/models"

	"github.com/stretchr/testify/assert"
)

func TestBuildLocales_NotEmpty(t *testing.T) {
	a := assert.New(t)

	translations, err := locales.BuildLocales()

	a.Nil(err)

	a.True(
		len(translations) > 0,
		"Translations must contain at least one locale",
	)
}

func TestBuildLocales_DefaultLocale(t *testing.T) {
	a := assert.New(t)

	translations, _ := locales.BuildLocales()

	haveDefaultLocale := false

	for _, localeTranslations := range translations {
		if localeTranslations.DefaultLocale {
			haveDefaultLocale = true

			break
		}
	}

	a.True(
		haveDefaultLocale,
		`Translations must contain "%s" locale`,
		locales.DefaultLocale,
	)
}

func TestBuildLocales_AllKeys(t *testing.T) {
	localeTranslations, err := locales.BuildLocales()
	if nil != err {
		t.Error(err)
	}

	var (
		defaultLocaleTranslationsExists bool
		defaultLocaleTranslations       models.LocaleTranslations
	)

	for _, translation := range localeTranslations {
		if translation.DefaultLocale {
			defaultLocaleTranslations = translation
			defaultLocaleTranslationsExists = true

			break
		}
	}

	if !defaultLocaleTranslationsExists {
		t.Errorf("Default locale translations not found")
	}

	for _, localeTranslation := range localeTranslations {
		if localeTranslation.DefaultLocale {
			continue
		}

		for _, expectedTranslation := range defaultLocaleTranslations.Translations {
			translationExists := false

			for _, localeTranslation := range localeTranslation.Translations {
				if localeTranslation.Key == expectedTranslation.Key {
					translationExists = true
					break
				}
			}

			if !translationExists {
				t.Errorf("Translation key %s for %s not found", expectedTranslation.Key, localeTranslation.Code)
			}
		}
	}
}
